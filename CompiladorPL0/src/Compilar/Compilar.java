package Compilar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import javax.swing.JFileChooser;

public class Compilar {

    public static void main(String[] args) {
        
        String nomArch = "";
        if (args.length == 1) {
            nomArch = args[0];
            llamadoCompilar(nomArch);
        } else {
            System.out.println("Error!\n");
            System.out.println("Uso: java -jar \"Compilar.jar\" <archivo>\n");
            System.out.println("Abro FileChooser, elija el archivo fuente desde allí\n");
            JFileChooser fc = new JFileChooser("");
            if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {

                nomArch = fc.getSelectedFile().getPath();
            }
            if (nomArch.isEmpty()) {
                System.out.println("Error!\n");
                System.out.println("Elija un archivo, o bien llame desde la consola como fue descripto arriba");
            } else {

                llamadoCompilar(nomArch);
            }
        }

    }

    public static void llamadoCompilar(String nomArch) {

        Reader archFuente = null;
        try {
            archFuente = new BufferedReader(new FileReader(nomArch));
            AnalizadorLexico aLex = new AnalizadorLexico(archFuente);
            IndicadorDeErrores indicadorDeErrores = new IndicadorDeErrores();
            AnalizadorSemantico aSem = new AnalizadorSemantico(indicadorDeErrores);
            GeneradorDeCodigo genCod = new GeneradorDeCodigo(nomArch, indicadorDeErrores);
            AnalizadorSintactico aSint = new AnalizadorSintactico(aLex, aSem, genCod, indicadorDeErrores);
            try {
                aSint.analizar();
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
            try {
                archFuente.close();
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        } catch (FileNotFoundException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
